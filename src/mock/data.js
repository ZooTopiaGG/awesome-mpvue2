/**
 *  mock data for demo ...
 *
 *  @date 2018-01-02
 *  @author jillyandkai@qq.com
 *  @decs 测试数据
 */
/* jshint esversion: 6 */

export default {
    // 一级分类
    catagoryTopList: [{
        id: 1,
        title: "雪糕榜单",
        icon: "<i-icon type=\"accessory\"/>"
    }, {
        id: 2,
        title: "基础护肤",
        icon: "<i-icon type=\"activity\"/>"
    }, {
        id: 3,
        title: "哥哥的超长个护美妆",
        icon: "<i-icon type=\"barrage\"/>"
    }, {
        id: 4,
        title: "身体护理",
        icon: "<i-icon type=\"browse\"/>"
    }, {
        id: 5,
        title: "美容工具",
        icon: "<i-icon type=\"computer\"/>"
    }, {
        id: 6,
        title: "男士个护",
        icon: "<i-icon type=\"coupons\"/>"
    }, {
        id: 7,
        title: "其他美妆",
        icon: "<i-icon type=\"dynamic\"/>"
    }],
    // 二级分类
    catagoryBelowList: [{
        id: 1,
        title: "妆前隔离超长标题",
        icon: "<i-icon type=\"accessory\"/>"
    }, {
        id: 2,
        title: "粉底气垫",
        icon: "<i-icon type=\"activity\"/>"
    }, {
        id: 3,
        title: "粉饼",
        icon: "<i-icon type=\"barrage\"/>"
    }, {
        id: 4,
        title: "蜜粉散粉",
        icon: "<i-icon type=\"browse\"/>"
    }, {
        id: 5,
        title: "BB霜",
        icon: "<i-icon type=\"computer\"/>"
    }, {
        id: 6,
        title: "遮瑕",
        icon: "<i-icon type=\"coupons\"/>"
    }, {
        id: 7,
        title: "修容",
        icon: "<i-icon type=\"dynamic\"/>"
    }, {
        id: 8,
        title: "唇妆",
        icon: "<i-icon type=\"dynamic\"/>"
    }, {
        id: 9,
        title: "腮红",
        icon: "<i-icon type=\"dynamic\"/>"
    }, {
        id: 10,
        title: "眉笔",
        icon: "<i-icon type=\"dynamic\"/>"
    }, {
        id: 11,
        title: "睫毛膏",
        icon: "<i-icon type=\"dynamic\"/>"
    }, {
        id: 12,
        title: "彩妆套装",
        icon: "<i-icon type=\"dynamic\"/>"
    }],
    // 商品列表
    goodList: [{
        id: 1,
        title: "MIKIMOTO COSMETICS 珍珠光彩养肤蜜粉",
        cover: "",
        cost: 400,
        original: 790
    }, {
        id: 2,
        title: "MIKIMOTO COSMETICS 大蚌贵妇美容液",
        cover: "",
        cost: 353,
        original: 790
    }, {
        id: 3,
        title: "MIKIMOTO COSMETICS 拉菲丽娜黑珍珠活肤精华霜",
        cover: "",
        cost: 260,
        original: 790
    }, {
        id: 4,
        title: "MIKIMOTO COSMETICS 拉菲丽娜黑珍珠活肤精华液",
        cover: "",
        cost: 560,
        original: 790
    }, {
        id: 5,
        title: "MIKIMOTO COSMETICS 温和防嗮SPF50+",
        cover: "",
        cost: 500,
        original: 790
    }, {
        id: 6,
        title: "MIKIMOTO COSMETICS 御木本拉菲丽娜黑珍珠活肤化妆水",
        cover: "",
        cost: 467,
        original: 790
    }]
};
