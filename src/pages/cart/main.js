/**
 * 购物车
 *
 * @author jillyandkai@qq.com
 * @date 2018-12-29
 */
/* jshint esversion: 6 */
import Vue from "vue";
import App from "./index";

const app = new Vue(App);
app.$mount();
