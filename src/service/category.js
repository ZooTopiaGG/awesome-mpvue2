/**
 *  category service
 *
 *  @date 2019-01-02
 *  @author jillyandkai@qq.com
 */
/* jshint esversion: 6 */
import data from "@/mock/data";

/**
 * 获取一级分类
 *
 * @returns {*}
 */
let getCategoryTopList = () => {
    // 加入接口
    return data.catagoryTopList;
};

/**
 * 获取二级分类
 *
 * @param categoryId 一级分类id
 * @returns {default.catagoryBelowList|{icon, id, title}}
 */
let getCategoryBelowList = (categoryId) => {
    // 加入接口
    return data.catagoryBelowList;
};

export default {
    getCategoryTopList,
    getCategoryBelowList
};
