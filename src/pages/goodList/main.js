/**
 * 商品列表
 *
 * @author jillyandkai@qq.com
 * @date 2018-01-03
 */
/* jshint esversion: 6 */
import Vue from "vue";
import App from "./index";

const app = new Vue(App);
app.$mount();
