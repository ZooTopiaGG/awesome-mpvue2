/*
 * @Author: dengpeng 
 * @Date: 2019-01-02 21:49:17 
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2019-01-02 22:20:04
 */

import data from "@/mock/home";

/**
 * 获取会场列表
 *
 * @returns {*}
 */
export const getVenueList = () => {
    // 加入接口
    return data.venueList;
};
