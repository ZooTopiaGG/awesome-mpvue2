/**
 *  mock data for demo ...
 *
 *  @date 2018-01-02
 *  @author jillyandkai@qq.com
 *  @decs 测试数据
 */
/* jshint esversion: 6 */

export default {
    // 一级分类
    venueList: [{
        id: 1,
        title: "CALBEE 卡乐比",
        icon: "<i-icon type=\"accessory\"/>"
    }, {
        id: 2,
        title: "Kabaya 卡巴也",
        icon: "<i-icon type=\"activity\"/>"
    }, {
        id: 3,
        title: "Kabohon",
        icon: "<i-icon type=\"barrage\"/>"
    }, {
        id: 4,
        title: "KAGOME 可果美",
        icon: "<i-icon type=\"browse\"/>"
    }, {
        id: 5,
        title: "KAI 贝印",
        icon: "<i-icon type=\"computer\"/>"
    }, {
        id: 6,
        title: "KAKUKYU 八顶味噌",
        icon: "<i-icon type=\"coupons\"/>"
    }, {
        id: 7,
        title: "KANEBO 佳丽宝",
        icon: "<i-icon type=\"dynamic\"/>"
    },{
        id: 8,
        title: "KANEMATSU 兼松",
        icon: "<i-icon type=\"dynamic\"/>"
    },{
        id: 9,
        title: "Kaneson",
        icon: "<i-icon type=\"dynamic\"/>"
    },{
        id: 10,
        title: "Kanesu制面",
        icon: "<i-icon type=\"dynamic\"/>"
    }],
};
