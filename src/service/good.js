/**
 *  good service
 *
 *  @date 2019-01-03
 *  @author jillyandkai@qq.com
 */
/* jshint esversion: 6 */
import data from "@/mock/data";

/**
 * 根据分类获取商品列表
 *
 * @returns {*}
 */
let getGoodListByCategoryId = (categoryId) => {
    // 加入接口
    return data.goodList;
};

export default {
    getGoodListByCategoryId
};
