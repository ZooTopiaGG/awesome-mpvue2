# awesome-mpvue

> A Mpvue project

## 项目说明
1）本项目基于vue，本机请安装
    node + @vue/cli (3.0以上版本)
    https://cli.vuejs.org/zh/guide/installation.html

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


