export default [{
    title: "首页",
    path: "../index/main",
    name: "home",
    icon: "homepage", // 暂时使用
    isMenu: true
}, {
    title: "分类",
    path: "../category/main",
    name: "category",
    icon: "label", // 暂时使用
    isMenu: true
}, {
    title: "购物车",
    path: "../cart/main",
    name: "cart",
    icon: "service", // 暂时使用
    isMenu: true
}, {
    title: "我的",
    path: "../mine/main",
    name: "mine",
    icon: "mine", // 暂时使用
    isMenu: true
}, {
    path: "*",
    title: "404",
    isMenu: false
}];
